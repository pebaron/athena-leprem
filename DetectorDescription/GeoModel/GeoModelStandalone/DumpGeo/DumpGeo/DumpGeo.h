/*
  Copyright (C) 2002-2024 CERN for the benefit of the ATLAS collaboration
*/

/////////////////////////////////////////////////////////////
//                                                         //
//  Header file for class DumpGeo                          // 
//                                                         //
//  Initial version:                                       //
//  - 2017, Sep -- Riccardo Maria BIANCHI                  //
//                 <riccardo.maria.bianchi@cern.ch>        //
//                                                         //
//  Main updates:                                          //
//  - 2024, Feb -- Riccardo Maria BIANCHI                  //
//                 <riccardo.maria.bianchi@cern.ch>        //
//                                                         //
//  This is the Athena algorithm to dump the geometry      //
//                                                         //
/////////////////////////////////////////////////////////////

#ifndef DumpGeo_DumpGeo
#define DumpGeo_DumpGeo

#include "AthenaBaseComps/AthAlgorithm.h"
#include "CxxUtils/checker_macros.h"
#include "GaudiKernel/IIncidentListener.h"

#include <string>
#include <vector>

class GeoExporter;

// Marked not thread-safe because GeoExporter uses VP1.
class ATLAS_NOT_THREAD_SAFE DumpGeo: public AthAlgorithm,
                                     public IIncidentListener
{
 public:
  DumpGeo(const std::string& name, ISvcLocator* pSvcLocator) ATLAS_CTORDTOR_NOT_THREAD_SAFE;
  ~DumpGeo()=default;

  StatusCode initialize();
  StatusCode execute();

  void handle(const Incident& inc);

 private:
  IToolSvc* m_toolSvc;
  GeoExporter* m_geoExporter;

  // Properties
  // -- Athena-related
  Gaudi::Property<std::string> m_atlasRelease{this, "AtlasRelease", "", "The current, in use Atlas release"}; 

};

#endif
